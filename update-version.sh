#!/usr/bin/env bash

IFS=$(echo -e "\n\b")
readonly VERSION_FILE_PATH=${VERSION_FILE_NAME:="$(dirname "$0")/version.txt"}
readonly PYPI_SETUP_FILE_PATHS=("${PYPI_SETUP_FILE_PATHS:=$(find "$(dirname "$0")" -maxdepth 2 -type f -name "setup.py")}")
readonly NPM_PACKAGE_JSON_FILE_PATHS=("${NPM_PACKAGE_JSON_FILE_PATHS:=$(find "$(dirname "$0")" -maxdepth 2 -name "package.json")}")
readonly GITLAB_CI=${GITLAB_CI:=false}
readonly COMMAND_NAME=$0
readonly SNIPPET_URL=${SNIPPET_URL:=https://gitlab.com/api/v4/snippets/2397526/raw}

OLD_VERSION=""
NEW_VERSION=""
DRY_RUN=false
UPDATE_MODE="UNKNOWN"

usage() {
	cat <<-EOF
		Script to update the version. The update can be one of the 3 types:
		 - Major: The first number will be increase of 1 and the 2 other will be set to 0
		 - Minor: The second number will be increase of 1 and the third number will be set to 0
		 - Patch: The third number will be increase of 1

		====== Example =====

			$COMMAND_NAME  major|minor|patch

		==== Option List ===

			One argument is always required. It have to be major, minor or patch. The case is insensitive.
			None are require if the script is launch from a gitlab runner.

			-h: Display this help message.
			-d: dry run

		====================
	EOF
}

parse_args() {
	local OPTIND # Without this variable declaration the function will have a incorrect behaviour on future calls
	local option
	local arguments=("$@")
	local argument_number=$#
	local tmp=""

	if [ "$#" -ge 1 ]; then
		if ! [[ "$1" =~ ^-.* ]]; then
			tmp=$1
			shift
		fi
	fi

	# Parse the arguments with the bash builtin 'getopts'
	while getopts "hd" option; do
		case ${option} in
		'd')
			DRY_RUN=true
			continue
			;;
		'h')
			usage
			exit 0
			;;
		'?')
			exit 2
			;;
		esac
	done

	if [ "$((argument_number - OPTIND))" -ne 0 ]; then
		echo 1>&2 "Error: Illegal number of parameters"
		exit 2
	fi
	if [ -z "$tmp" ]; then
		UPDATE_MODE=$(echo "${!OPTIND}" | tr '[:upper:]' '[:lower:]')
	else
		UPDATE_MODE=$(echo "$tmp" | tr '[:upper:]' '[:lower:]')
	fi
	if [[ $UPDATE_MODE != "major" && $UPDATE_MODE != "minor" && $UPDATE_MODE != "patch" ]]; then
		echo 1>&2 "Error: Unsupported mode of update"
		exit 2
	fi
}

update_version_number() {
	local IFS='.'
	read -r -a numbers < <(echo "$OLD_VERSION")

	if [[ $UPDATE_MODE == "major" ]]; then
		numbers[0]=$((numbers[0] + 1))
		numbers[1]=0
		numbers[2]=0
		NEW_VERSION="${numbers[*]}"
	elif [[ $UPDATE_MODE == "minor" ]]; then
		numbers[1]=$((numbers[1] + 1))
		numbers[2]=0
		NEW_VERSION="${numbers[*]}"
	elif [[ $UPDATE_MODE == "patch" ]]; then
		numbers[2]=$((numbers[2] + 1))
		NEW_VERSION="${numbers[*]}"
	else
		echo 1>&2 "Error: Trying with unsupported mode of update"
		exit 2
	fi
}

discover_update_mode() {
	local IFS='.'
	read -r -a old_numbers < <(echo "$OLD_VERSION")
	read -r -a new_numbers < <(echo "$NEW_VERSION")

	if [ "${new_numbers[0]}" -gt "${old_numbers[0]}" ]; then
		UPDATE_MODE="major"
	elif [ "${new_numbers[1]}" -gt "${old_numbers[1]}" ]; then
		UPDATE_MODE="minor"
	elif [ "${new_numbers[2]}" -gt "${old_numbers[2]}" ]; then
		UPDATE_MODE="patch"
	fi
}


update_supported_files() {

	if ! $DRY_RUN; then

		echo "$NEW_VERSION" >"$VERSION_FILE_PATH"

		for file_name in ${PYPI_SETUP_FILE_PATHS[*]}; do
			sed -i -e "s/version=\".*\"/version=\"$NEW_VERSION\"/g" "$file_name"
		done

		for file_name in ${PACKAGE_JSON_FILE_PATHS[*]}; do
			sed -i -e "s/\"version\": \".*\"/\"version\": \"$NEW_VERSION\"/g" "$file_name"
		done
	fi

	echo "Supported files updated from $OLD_VERSION to $NEW_VERSION"
}


commit_updated_files() {

	set -e
	git_dry_options=""
	if $DRY_RUN; then
		git_dry_options="--dry-run"
	fi

	# shellcheck disable=SC2086
	git -c core.safecrlf='false' add "$VERSION_FILE_PATH" ${PYPI_SETUP_FILE_PATHS[*]} ${NPM_PACKAGE_JSON_FILE_PATHS[*]} $git_dry_options

	# git ls-files options:
	#                 -o:  for the new files added to the repo
	#                 -m:  for the files already in the repo but updated
	# --exclude-standard:  to exclude the file listed in .gitignore
	nbr_files_untracked=$(git ls-files -om --exclude-standard | wc -l)
	if [ "$nbr_files_untracked" -eq "0" ]; then
		git commit -m "Version $NEW_VERSION" $git_dry_options
		git tag $NEW_VERSION -m "version $NEW_VERSION" $git_dry_options
	else
		echo 1>&2 "Errors: Your git repository isn't clean ($nbr_files_untracked untracked files)"
		git ls-files -om --exclude-standard
		# shellcheck disable=SC2086
		git reset "$VERSION_FILE_PATH" ${PYPI_SETUP_FILE_PATHS[*]} ${NPM_PACKAGE_JSON_FILE_PATHS[*]} >/dev/null
		exit 2
	fi
}

# Check if we sourced the file or try to run it.
if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then

	# The file is not sourced so we can execute the code.

  # Some simple warnings
  if [[ $(basename "$0") != "update-version.sh" ]]; then
    echo 1>&2 "Warning: the script name isn't update-version.sh"
  fi

  file_content_local=$(md5sum < "$0")
  file_content_latest=$(curl -m1 -s "$SNIPPET_URL" | md5sum)
  if [[ "$file_content_latest" != "$file_content_local" ]]; then
    echo 1>&2 "Warning: the script isn't to the latest version"
  fi

	if $GITLAB_CI; then

		echo "Running inside a gitlab runner"

		UPDATE_MODE="UNKNOWN"

		OLD_VERSION=$(git show HEAD~1:"$VERSION_FILE_PATH")
		NEW_VERSION=$(cat "$VERSION_FILE_PATH")
		discover_update_mode

		if [[ $UPDATE_MODE == "UNKNOWN" ]] && [[ $CI_COMMIT_BRANCH == "master" ]] &&
			! [[ $CI_COMMIT_TITLE =~ ^Version\ [0-9]+\.[0-9]+\.[0-9]+$ ]]; then

			git config --local user.email "public-gitlab@devo.live"
			git config --local user.name "GitLab Runner"

			git remote rm origin
			git remote add origin "git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git"

			UPDATE_MODE="patch"
			update_version_number
			update_supported_files
			commit_updated_files

			# shellcheck disable=SC2174
			mkdir -p -m 0700  ~/.ssh
			echo -en "Host $CI_SERVER_HOST\n IdentityFile  ~/.ssh/id_rsa" > ~/.ssh/config
			echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
			chmod 644 ~/.ssh/known_hosts
			echo "$SSH_PRIVATE_KEY_GITLAB_COM" > ~/.ssh/id_rsa
			chmod 600 ~/.ssh/id_rsa

			git push origin HEAD:"$CI_COMMIT_BRANCH" --tags

			rm ~/.ssh/id_rsa
		else
			echo "An update already have been done previously"
		fi

	else
		parse_args "$@"
		OLD_VERSION=$(cat "$VERSION_FILE_PATH")
		update_version_number
		update_supported_files
		commit_updated_files
	fi
fi
