import sys
import unittest

from super_task import my_super_task, my_super_task_with_param

from duration_check import duration, timeout_decorator_builder


def module_getter():
    return sys.modules[__name__]


timeout = timeout_decorator_builder(module_getter)


class SuperTaskUnitTest(unittest.TestCase):

    @timeout(2)
    def test_timeout_super_task(self):
        self.assertTrue(my_super_task())

    @timeout(3)
    def test_timeout_super_task_with_param_2(self):
        self.assertTrue(my_super_task_with_param(2))

    @timeout(5)
    def test_timeout_super_task_with_param_4(self):
        self.assertTrue(my_super_task_with_param(4))

    @timeout(0.7)
    def assert_timeout_super_task(self):
        self.assertTrue(my_super_task())

    @timeout(1.5)
    def assert_timeout_super_taskwith_param_2(self):
        self.assertTrue(my_super_task_with_param(1.8))

    @timeout(3.5)
    def assert_timeout_super_taskwith_param_4(self):
        self.assertTrue(my_super_task_with_param(4))

    def test_assert_timeout(self):

        self.assertRaises(AssertionError, self.assert_timeout_super_task)
        self.assertRaises(AssertionError, self.assert_timeout_super_taskwith_param_2)
        self.assertRaises(AssertionError, self.assert_timeout_super_taskwith_param_4)

    ##################
    #    Duration    #
    ##################

    @duration(1)
    def test_duration_super_task(self):
        self.assertTrue(my_super_task())

    @duration(3)
    def test_duration_super_task_with_param_4(self):
        self.assertTrue(my_super_task_with_param(3))

    @duration(1.5)
    def assert_duration_super_task(self):
        self.assertTrue(my_super_task)

    @duration(2.2)
    def assert_duration_super_task_with_param_2(self):
        self.assertTrue(my_super_task_with_param(2))

    @duration(5)
    def assert_duration_super_task_with_param_4(self):
        self.assertTrue(my_super_task_with_param(4.1))

    def test_assert_duration(self):
        self.assertRaises(AssertionError, self.assert_duration_super_task)
        self.assertRaises(AssertionError, self.assert_duration_super_task_with_param_2)
        self.assertRaises(AssertionError, self.assert_duration_super_task_with_param_4)

