import sys

from super_task import my_super_task, my_super_task_with_param

from duration_check import timeout_decorator_builder


def module_getter():
    return sys.modules[__name__]


timeout = timeout_decorator_builder(module_getter)


@timeout(2)
def normal_root_function1():
    my_super_task()


@timeout(5)
def normal_root_function2(value: int):
    my_super_task_with_param(value)


@timeout(5, verbose=True)
def normal_root_function3(useless, value: int):
    my_super_task_with_param(value)


if __name__ == '__main__':

    normal_root_function1()
    normal_root_function2(2)
    normal_root_function3("useless", 3)
    normal_root_function3(value=4, useless="useless")
