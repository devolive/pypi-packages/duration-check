import sys

from duration_check import timeout_decorator_builder
from super_task import my_super_task, my_super_task_with_param


def module_getter():
    return sys.modules[__name__]


timeout = timeout_decorator_builder(module_getter)


class SuperTaskWrapper:

    @timeout(2)
    def test_super_task(self):
        my_super_task()

    @timeout(1.5)
    def test_super_task_with_param(self):
        my_super_task_with_param(2)


if __name__ == '__main__':

    spw1 = SuperTaskWrapper()
    spw1.test_super_task()
    spw1.test_super_task_with_param()

    spw2 = SuperTaskWrapper()
    spw2.test_super_task()
    spw2.test_super_task_with_param()
