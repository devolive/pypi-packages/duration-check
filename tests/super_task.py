from time import sleep
from typing import Union

def my_super_task():
    print("My super long task of 1 second")
    sleep(1)
    return True


def my_super_task_with_param(waiting_time: Union[int, float] = 3):
    print(f"My super long task of {waiting_time} seconds")
    sleep(waiting_time)
    return True