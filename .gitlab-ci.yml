stages:
  - tests
  - update_version
  - build
  - upload
  - release

variables:
  PACKAGE_ID: "duration_check"
  REGISTRY_ROOT_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"

########################################################################################################################
#                                                         Tests                                                        #
########################################################################################################################

unit_test_python:
  stage: tests
  image: python:3
  script:
    - pip install -r requirements.txt
    - cd tests
    - PYTHONPATH=.. python -m unittest
  only:
    - branches


########################################################################################################################
#                                                    Update version                                                    #
########################################################################################################################


update_version_number:
  stage: update_version
  image: python:3
  needs:
    - unit_test_python
  script:
    - ./update-version.sh
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master"
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /Version [0-9]+\.[0-9]+\.[0-9]+/


########################################################################################################################
#                                                        Build                                                         #
########################################################################################################################


build_pypi_package:
  stage: build
  image: python:3
  needs:
    - unit_test_python
  script:
    - python setup.py sdist
  artifacts:
    paths:
      - dist/
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master" && $CI_COMMIT_MESSAGE =~ /Version [0-9]+\.[0-9]+\.[0-9]+/

########################################################################################################################
#                                                        Upload                                                        #
########################################################################################################################

upload_to_pypi.org:
  stage: upload
  image: python:3
  needs:
    - job: build_pypi_package
      artifacts: true
  script:
    - ls dist
    - pip install -U twine
    - twine upload dist/*
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master" && $CI_COMMIT_MESSAGE =~ /Version [0-9]+\.[0-9]+\.[0-9]+/


upload_artifacts_to_gitlab.com:
  stage: upload
  image: curlimages/curl:latest
  needs:
    - job: build_pypi_package
      artifacts: true
  script:
    - ls dist
    - export PACKAGE_VERSION=${CI_COMMIT_TITLE/"Version "/""}
    - export PACKAGE_NAME="${PACKAGE_ID}-${PACKAGE_VERSION}.tar.gz"
    - export ASSET_URL="${REGISTRY_ROOT_URL}/${PACKAGE_ID}/${PACKAGE_VERSION}/${PACKAGE_NAME}"
    - echo "Uploading $PACKAGE_NAME to ($ASSET_URL)"
    - |
      curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file dist/${PACKAGE_NAME} ${ASSET_URL}
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master" && $CI_COMMIT_MESSAGE =~ /Version [0-9]+\.[0-9]+\.[0-9]+/


########################################################################################################################
#                                                       Release                                                        #
########################################################################################################################


create_gitlab_release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - upload_artifacts_to_gitlab.com
  script:
    - export PACKAGE_VERSION=${CI_COMMIT_TITLE/"Version "/""}
    - export PACKAGE_NAME="${PACKAGE_ID}-${PACKAGE_VERSION}.tar.gz"
    - export ASSET_URL="${REGISTRY_ROOT_URL}/${PACKAGE_ID}/${PACKAGE_VERSION}/${PACKAGE_NAME}"
    - echo "Using asset from $ASSET_URL"
    - |
      release-cli create --name "Release from runner (v${PACKAGE_VERSION})" --tag-name "${PACKAGE_VERSION}" \
        --assets-link "{\"name\":\"${PACKAGE_NAME}\",\"url\":\"${ASSET_URL}\",\"type\":\"package\"}"
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master" && $CI_COMMIT_MESSAGE =~ /Version [0-9]+\.[0-9]+\.[0-9]+/
